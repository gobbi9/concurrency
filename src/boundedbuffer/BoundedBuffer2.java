package boundedbuffer;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BoundedBuffer2 {
	private String[] buffer;
	private int ptr;

	private final Lock lock = new ReentrantLock();
	private final Condition producers = lock.newCondition();
	private final Condition consumers = lock.newCondition();

	public BoundedBuffer2(int capacity) {
		buffer = new String[capacity];
		ptr = 0;
	}

	public void deposit(Runnable r) {
		lock.lock();
		try {
			while (ptr >= buffer.length)
				producers.await();

			buffer[ptr++] = r.toString();
			System.out.print(this);
			consumers.signalAll();
		}
		catch (Exception e) {
		}
		finally {
			lock.unlock();
		}
	}

	public String withdraw() {
		lock.lock();
		try {
			while (ptr < 0)
				consumers.await();
			String s = buffer[ptr--];
			buffer[ptr+1] = null;
			System.out.print(this);
			producers.signalAll();
			return s;
		}
		catch (Exception e) {
			return "error";
		}
		finally {
			lock.unlock();
		}
	}

	public synchronized String toString() {
		String out = "\n";
		for (int i = 0; i < buffer.length; i++) {
			if (buffer[i] != null)
				out += i + ": " + buffer[i] + ", ";
		}
		return out;

	}

}
