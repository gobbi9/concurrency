package boundedbuffer;

public class Producer implements Runnable{

	private int id;
	
	public Producer(int id){
		this.id = id;
	}
	
	public void run() {
		while (true){
			Main.buffer.deposit(this);
		}
	}
	
	public String toString(){
		return "P"+id;
	}

}
