package boundedbuffer;

public class Consumer implements Runnable{

	private int id;
	
	public Consumer(int id){
		this.id = id;
	}
	
	public void run() {
		while (true){
			Main.buffer.withdraw();
		}
	}
	
	public String toString(){
		return "C"+id;
	}

}
