package boundedbuffer;

public class BoundedBuffer {
	private String[] buffer;
	private int ptr;
	
	public BoundedBuffer(int capacity){
		buffer = new String[capacity];
		ptr = 0;
	}
		
	public synchronized void deposit(Runnable r){
		while (ptr >= buffer.length) //its full
			try {
				wait();
			}catch (InterruptedException e) {}
		
		buffer[ptr++] = r.toString();
		System.out.print(this);
		notifyAll();
	}
	
	public synchronized String withdraw(){
		while (ptr < 0) //its empty
			try {
				wait();
			}catch (InterruptedException e) {}
		
		String s = buffer[ptr--];
		buffer[ptr+1] = null;
		System.out.print(this);
		notifyAll();
		return s; 
	}
	
	public synchronized String toString(){
		String out = "\n";
		for (int i = 0; i < buffer.length; i++){
			if (buffer[i] != null)
				out += i + ": " + buffer[i] + ", ";
		}
		return out;
	}
	
}
