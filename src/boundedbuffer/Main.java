package boundedbuffer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
	
	public static BoundedBuffer2 buffer;
	public static List<Consumer> consumers;
	public static List<Producer> producers;
	
	public static final int SIZE = 20;
	public static final int SIZE_P = 20;
	public static final int SIZE_C = 20;
	
	
	public static void main(String[] args) {
		buffer = new BoundedBuffer2(SIZE);
		consumers = new ArrayList<Consumer>();
		producers = new ArrayList<Producer>();
		
		for (int i = 0; i < SIZE_C; i++)
			consumers.add(new Consumer(i));
		
		for (int i = 0; i < SIZE_P; i++)
			producers.add(new Producer(i));
		
		List<Runnable> entry = new ArrayList<Runnable>();
		entry.addAll(consumers);
		entry.addAll(producers);
		Collections.shuffle(entry);
		
		for (Runnable r : entry)
			System.out.print(r+" ");
		
		System.out.println();
		
		List<Thread> threads = new ArrayList<Thread>();
		
		for (Runnable r : entry)
			threads.add(new Thread(r));
		
		for (Thread t : threads)
			t.start();
		
		for (Thread t : threads)
			try {
				t.join();
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		
		System.out.println("End.");
	}

}
