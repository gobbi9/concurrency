package channels;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * assynchronous mailbox with buffer blocking sending and blocking receiving
 */
public class Mailbox implements Channel {
	private int capacity;
	private Queue<Object> buffer;
	private final Lock mutex = new ReentrantLock();
	private final Condition senders = mutex.newCondition();
	private final Condition receivers = mutex.newCondition();

	public Mailbox() {
		this(10);
	}

	public Mailbox(int capacity) {
		this.capacity = capacity;
		buffer = new LinkedList<Object>();
	}

	public boolean send(Object message) {
		mutex.lock();
		try {
			while (buffer.size() == capacity)
				senders.await();

			buffer.add(message);
			receivers.signal();
			return true;
		}
		catch (Exception e) {
			return false;
		}
		finally{
			mutex.unlock();
		}
	}

	public Object receive() {
		mutex.lock();
		try {
			while (buffer.size() == 0)
				receivers.await();
			
			Object o = buffer.poll();
			senders.signal();
			return o;
		}
		catch (Exception e) {
			return null;
		}
		finally{
			mutex.unlock();
		}
	}
}
