package channels;

public interface Channel {
	public abstract boolean send(Object message);
	public abstract Object receive();
}
