package barbershop;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static BarberShop barberShop;

	public static void main(String[] args) {
		Barber barber = new Barber();
		barberShop = new BarberShop(barber);

		List<Thread> threads = new ArrayList<Thread>();
		for (int i = 0; i < BarberShop.MAX_CLIENTS + 10; i++)
			threads.add(new Thread(new Client(i)));

		new Thread(barber).start();
		for (Thread t : threads)
			t.start();

		try {
			for (Thread t : threads)
				t.join();
		}
		catch (Exception e) {
		}
	}
}
