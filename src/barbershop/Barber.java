package barbershop;

public class Barber implements Runnable {

	public enum State {
		BUSY, SLEEPING;
	};

	public State state;

	public Barber() {
		state = State.SLEEPING;
	}

	public void run() {
		while (true) {
			System.out.println("1: " + this);
			Main.barberShop.attendCustomer();
			System.out.println("2: " + this);
		}
	}

	public synchronized String toString() {
		return String.format("%s (%s)", "B", state.name());
	}
}
