package barbershop;


public class Client implements Runnable{
	private int id;
	
	public Client(int id){
		this.id = id;
	}
	
	public void run() {
		Main.barberShop.enterShop();
		System.out.println(this);
	}
	
	public synchronized String toString(){
		return "C"+id;
	}
}
