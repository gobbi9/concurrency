package barbershop;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import barbershop.Barber.State;

public class BarberShop {
	private final Lock lock = new ReentrantLock();
	private final Condition clients = lock.newCondition();
	private final Condition barber = lock.newCondition();
	public static final int MAX_CLIENTS = 10 + 1;
	private int numClients;
	private Barber b;
	
	public BarberShop(Barber b){
		numClients = 0;
		this.b = b;
	}
	
	public void attendCustomer(){ //called by barber
		lock.lock();
		try{
			if (numClients == 0){
				b.state = State.SLEEPING;
				barber.await();
			}
			
			b.state = State.BUSY;
			clients.signal();
		}catch(Exception e){}
		finally{
			lock.unlock();
		}
		
	}
	
	public void enterShop(){ //called by client
		lock.lock();
		
		try{
			if (numClients < MAX_CLIENTS){
				numClients++;
				if (b.state == State.BUSY){
					clients.await();
				}
				else if (b.state == State.SLEEPING){
					barber.signal();
				}
				numClients--;
			}	
				
		}catch(Exception e){}
		finally{
			lock.unlock();
		}
	}
	
}
