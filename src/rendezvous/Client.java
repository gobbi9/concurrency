package rendezvous;

public class Client implements Runnable {
	private int id;

	public Client(int id) {
		this.id = id;
	}

	public void run() {
		Main.server.accept(this);
	}

	public synchronized String toString() {
		return "C" + id;
	}
}
