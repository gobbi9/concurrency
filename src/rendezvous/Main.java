package rendezvous;

import java.util.ArrayList;
import java.util.List;

public class Main {
	public static Server server;
	
	public static void main(String[] args) {
		server = new Server();
		
		List<Thread> threads = new ArrayList<Thread>();
		for (int i = 1; i <= 10; i++)
			threads.add(new Thread(new Client(i)));
		
		Thread s = new Thread(server);
		s.start();
		
		for (Thread t : threads)
			t.start();
		
		for (Thread t : threads)
			try {
				t.join();
			}
			catch (InterruptedException e) {
			}
		
		System.out.println("end");
	}
}
