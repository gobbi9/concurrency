package rendezvous;

import java.util.LinkedList;
import java.util.Queue;

public class Server implements Runnable {
	private Queue<Object> entry;
	private boolean processed;
	
	public Server() {
		entry = new LinkedList<Object>();
		processed = true;
	}

	public synchronized void run() {
		while (true){
			if (processed)
				notify();
		}
	}

	public synchronized Object accept(Object request) {
		// implement a channel to complete the class
		//processed = false;
		entry.add(request);
		try {
			wait();
			Client c = (Client) request;
			System.out.println("Processed: " + c);
			processed = true;
			return entry.poll();
		}
		catch (InterruptedException e) {
			return null;
		}	
		
	}

}
