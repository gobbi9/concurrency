package class2;

import class1.Main;

public class Thread1 extends Thread{
	private int myId;
	
	public Thread1(int myId){
		this.myId = myId;
	}
	
	public void run(){
		for (int i = 0; i<2000; i++) {
			Main.intendToEnter1 = true;
			Main.turn = 0;
			while (Main.intendToEnter0 && Main.turn == 0);
			//critical section
			
			System.out.printf("[%d (%d)]\n", myId, Main.c);
			Main.c++;
			
			Main.intendToEnter1 = false;
			//noncritical section
		}
		/*
		for (int i = 0; i<2000; i++){
			System.out.printf("[%d (%d)]\n", myId, Main.c);
			if (i % 10 == 0){
				System.out.println();
			}
			Main.c++;
		}
		*/
	}
}