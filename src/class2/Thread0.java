package class2;

import class1.Main;

public class Thread0 extends Thread{
	private int myId;
	
	public Thread0(int myId){
		this.myId = myId;
	}
	
	public void run(){
		for (int i = 0; i<2000; i++) {
			Main.intendToEnter0 = true;
			Main.turn = 1;
			while (Main.intendToEnter1 && Main.turn == 1);
			//critical section
			
			System.out.printf("[%d (%d)]\n", myId, Main.c);
			Main.c++;
			
			Main.intendToEnter0 = false;
			//noncritical section
		}
		/*
		for (int i = 0; i<2000; i++){
			System.out.printf("[%d (%d)]\n", myId, Main.c);
			if (i % 10 == 0){
				System.out.println();
			}
			Main.c++;
		}*/
	}
}
