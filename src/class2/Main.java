package class2;

import java.util.concurrent.atomic.AtomicInteger;

public class Main {
	public static int[] tickets;
	public static volatile AtomicInteger next; 
	public static volatile int permit; 
	
	public static void main(String[] args) {
		next = new AtomicInteger();
		tickets = new int[10];
		
		for (int i = 0; i < 10; i++)
			new TThread(i).start();
	}
}
