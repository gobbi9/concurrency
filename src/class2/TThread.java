package class2;

import class2.Main;

public class TThread extends Thread{
	private int myId;
	
	public TThread(int myId){
		this.myId = myId;
	}
	
	public void run(){
		Main.next.set(0);
		Main.permit = 0;
		
		while (true){
			Main.tickets[myId] = Main.next.getAndIncrement();
			while (Main.tickets[myId] != Main.permit);
			System.out.println(this); //critical section
			++Main.permit;
		}
	}
	
	public String toString(){
		return "T" + myId;
	}
}