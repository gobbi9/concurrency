package unisexbathroom;

public class Woman implements Runnable{
	private static int id = 0;
	
	public Woman(){
		id++;
	}
	
	public void run(){
		Main.bathroom.enterWoman();
		Main.bathroom.exitWoman();
	}
	
	public String toString(){
		return "W"+id;
	}
}
