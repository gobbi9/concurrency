package unisexbathroom;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@SuppressWarnings("all")
public class Bathroom {
	private Lock mutex = new ReentrantLock();
	private Condition men = mutex.newCondition();
	private Condition women = mutex.newCondition();
	
	private final int CAPACITY = 1;
	
	public Bathroom(){
		
	}
	
	public void enterMan(){
		
	}
	
	public void exitMan(){
		
	}
	
	public void enterWoman(){
		
	}
	
	public void exitWoman(){
		
	}
	

}
