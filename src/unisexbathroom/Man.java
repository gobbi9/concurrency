package unisexbathroom;

public class Man implements Runnable{
	private static int id = 0;
	
	public Man(){
		id++;
	}
	
	public void run(){
		while (true){
			Main.bathroom.enterMan();
			Main.bathroom.exitMan();
		}
	}
	
	public String toString(){
		return "M"+id;
	}
}
