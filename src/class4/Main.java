package class4;

import java.util.concurrent.Semaphore;

public class Main {
	public static int x = 0;
	public static Semaphore m = new Semaphore(1);
	public static void main(String[] args) {
		try {
			m.acquire();
		} catch (InterruptedException e) {
			System.out.println("error");
		}
		
		for (int i = 0; i<100; i++){
			MyThread t = new MyThread(i);
			t.start();
		}
			
		m.release();
		
	}
}
