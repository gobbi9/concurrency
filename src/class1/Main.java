package class1;

import java.util.concurrent.Semaphore;

public class Main {
	public static int x = 0, c = 1;
	public static volatile boolean intendToEnter0;
	public static volatile boolean intendToEnter1;
	public static volatile int turn;
	public static final int NUM_THREADS = 10;
	public static int[] array = new int[NUM_THREADS];
	public static ThreadTest[] vThreads;
	
	public static Semaphore m = new Semaphore(1);
	
	public static volatile boolean[] waiting = new boolean[NUM_THREADS];
	public static long lock = 0, key = 0;
	
	public static void main(String[] args) {
		/*Thread0 t0 = new Thread0(0);
		Thread1 t1 = new Thread1(1);
		t0.run();
		t1
		.run();
		for (int i=0; i<waiting.length; i++)
			waiting[i] = false;
		*/
		
		t2();
		
		for (int i = 0; i < NUM_THREADS; i++){
			try {
				vThreads[i].join();
			} catch (InterruptedException e1) {
				System.out.println("Join error");
				e1.printStackTrace();
			}
		}
		
		for (int e : array)
			System.out.print(e + " ");
		
		System.out.println("Length: " + array.length);
		
	}
	
	public static void t2(){
		/*ThreadTest t1 = new ThreadTest(1);
		ThreadTest t2 = new ThreadTest(2);
		ThreadTest t3 = new ThreadTest(3);
		
		t1.start();
		t2.start();
		t3.start();*/
		vThreads = new ThreadTest[NUM_THREADS];
		for (int i = 0; i < NUM_THREADS; i++){
			vThreads[i] = new ThreadTest(i);
			vThreads[i].start();
		}
	}
	
	public static void t1(){
		
		Thread a = new Thread(new Runnable(){
			public void run(){
				while (true){
					x = 1;
					System.out.println(x);
				}
			}
		});
		Thread b = new Thread(new Runnable(){
			public void run(){
				while (true){
					x = 2;
					System.out.println(x);
				}
			}
		});
		Thread c = new Thread(new Runnable(){
			public void run(){
				while (true)
					if (x == 2){
						System.out.println(x);
						System.exit(0);
					}
			}
		});
				
		a.start();
		b.start();
		c.start();
	}
}
