package class1;

public class ThreadTest extends Thread {
	private int myId;

	public ThreadTest(int myId) {
		this.myId = myId;
	}

	public void run() {
		/*
		 * for (int i = 0; i<20; i++){ System.out.printf("[%d (%d)]\n", myId,
		 * Main.c); Main.c++; }
		 */

		try {
			Main.m.acquire();
			for (int i = 0; i<20; i++){
				System.out.printf("[%d (%d)]\n", myId, Main.c);
				Main.c++;
			}
			Main.array[myId] = myId;
		} catch (Exception e) {
		} finally {
			Main.m.release();
		}
	}
}
