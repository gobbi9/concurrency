package class1;

public class AnotherTest implements Runnable{
	private int myId;
	
	public AnotherTest (int myId){
		this.myId = myId;
	}
	
	public void run() {
		for (int i = 0; i<20; i++){
			System.out.printf("Thread: %d,  counter: %d] ", myId, Main.c);
			if (i % 10 == 0)
				System.out.println();
			Main.c++;
		}
	}

}
